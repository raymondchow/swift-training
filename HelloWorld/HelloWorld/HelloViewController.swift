//
//  HelloViewController.swift
//  HelloWorld
//
//  Created by Raymond Chow Chee Seng on 30/06/2016.
//  Copyright © 2016 Raymond Chow Chee Seng. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {
    
    var label001: UILabel!
    var button001: UIButton!
    var button002: UIButton!
    var elementPicker1: UIPickerView!
    var valuePicker1: UIPickerView!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.brownColor()
        
        label001 = UILabel(frame: CGRect(x: 10, y: 20, width: 200, height: 20 ))
        label001.text = "First Hello World"
        label001.backgroundColor = UIColor.blueColor()
        self.view.addSubview(label001)
        
        button001 = UIButton(frame: CGRect(x: 10, y: 50, width: 80, height: 20))
        button001.setTitle("Hello", forState: UIControlState.Normal)
        button001.addTarget(self, action: #selector(HelloViewController.sayHi), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(button001)
        
        button002 = UIButton(frame: CGRect(x: 120, y: 50, width: 80, height: 20))
        button002.setTitle("Bye", forState: UIControlState.Normal)
        button002.addTarget(self, action: #selector(HelloViewController.sayBye), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button002)
        /*
        enum Elements: String {
            case Spade = "Spade"
            case Heart = "Heart"
            case Club = "Club"
            case Diamond = "Diamond"
            
        }
        */
        /*
        enum Values: String {
            case Ace = "Ace"
            case Two = "Two"
            case Three = "Three"
            case Four = "Four"
            case Five = "Five"
            case Six = "Six"
            case Seven = "Seven"
            case Eight = "Eight"
            case Nine = "Nine"
            case Ten = "Ten"
            case Jack = "Jack"
            case Queen = "Queen"
            case King = "King"
            
        }
         */
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sayHi()
    {
        label001?.text = "HI HI HI"
        
    }
    
    func sayBye()
    {
        label001?.text = "BYE, BYE, BYE"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
